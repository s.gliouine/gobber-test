Make sure you have installed the following software:

* Apache Maven 3.6
* JDK 17
* Docker for Desktop(for Windows users and MacOS users)
Let's start up a MongoDB server, you can simplify start it via [`docker-compose.yml`](https://github.com/soufianeGL/spring-reactive-jwt-sample/blob/master/docker-compose.yml) file.

```bash
docker-compose up mongodb
```

Execute the following command to start up the application

```bash
mvn clean spring-boot:run
```

Or run the `Application` class in your IDEs directly.

Let's start to test the APIs via `curl` command.

```bash
 curl -X POST http://localhost:8080/auth/login -H "Content-Type:application/json" -d "{\"username\":\"user\", \"password\":\"password\"}"
{"access_token":"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyIiwicm9sZXMiOiJST0xFX1VTRVIiLCJpYXQiOjE1OTA5MTE0ODIsImV4cCI6MTU5MDkxNTA4Mn0.lqsWeWEx9pkgg1xGfghpnKV7PkrgEb7R0FOeWrDQuF0"}
```

Try to fetch the current user info.

```bash
curl http://localhost:8080/me -H "Authorization:Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VyIiwicm9sZXMiOiJST0xFX1VTRVIiLCJpYXQiOjE1OTA5MTE0ODIsImV4cCI6MTU5MDkxNTA4Mn0.lqsWeWEx9pkgg1xGfghpnKV7PkrgEb7R0FOeWrDQuF0"
{"name":"user","roles":["ROLE_USER"]}
```

Grab the [source codes](https://github.com/soufianeGL/spring-reactive-jwt-sample) from my Github account, and explore it freely.